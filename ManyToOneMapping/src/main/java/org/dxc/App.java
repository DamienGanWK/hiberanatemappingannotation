package org.dxc;

import org.dxc.entity.Address;
import org.dxc.entity.Employee;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

/**
 * Hello world!
 *
 */
public class App {
	private static SessionFactory factory;

	public static void main(String[] args) {
		try {
			factory = new Configuration().configure("resource/hibernate.cfg.xml").buildSessionFactory();
		} catch (Throwable e) {
			System.err.println("Failed to create Session");
			throw new ExceptionInInitializerError(e);
		}
		Session session = factory.openSession();

		Transaction tx = session.beginTransaction();

		Employee e1 = new Employee();
		e1.setName("Ravi Malik");
		e1.setEmail("ravi@gmail.com");

		Employee e2 = new Employee();
		e2.setName("Damien");
		e2.setEmail("damien@gmail.com");

		Address address1 = new Address();
		address1.setAddressLine1("Choa Chu Kang");
		address1.setCity("Singapore");
		address1.setState("Northwest");
		address1.setCountry("Singapore");
		address1.setPincode(680447);

		e1.setAddress(address1);
		e2.setAddress(address1);

		session.persist(e1);
		session.persist(e2);

		tx.commit();
		session.close();
		System.out.println("success");
	}
}
