package org.dxc;


import java.util.ArrayList;

import org.dxc.entity.Answer;
import org.dxc.entity.Question;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 * Hello world!
 *
 */
public class App {
	private static SessionFactory factory;

	public static void main(String[] args) {
		try {
			// factory obj created
			factory = new Configuration().configure("resource/hibernate.cfg.xml").buildSessionFactory();
		} catch (Throwable e) {
			System.err.println("Failed to create Session");
			throw new ExceptionInInitializerError(e);
		}
		Session session = factory.openSession();
		org.hibernate.Transaction tx = session.beginTransaction();
		
		Answer ans1 = new Answer();
		ans1.setAnswername("Java is a language");
		ans1.setPostedBy("John");
		
		Answer ans2 = new Answer();
		ans2.setAnswername("Java is a platform");
		ans2.setPostedBy("Jack");
		
		Answer ans3 = new Answer();
		ans3.setAnswername("Servlet is an interface");
		ans3.setPostedBy("Sam");
		
		Answer ans4 = new Answer();
		ans4.setAnswername("JDBC is an API");
		ans4.setPostedBy("Ken");
		
		Answer ans5 = new Answer();
		ans5.setAnswername("Java supports OOPS");
		ans5.setPostedBy("Leon");
		
		ArrayList<Answer> list1 = new ArrayList<Answer>();
		list1.add(ans1);list1.add(ans2);list1.add(ans5);
		
		ArrayList<Answer> list2 = new ArrayList<Answer>();
		list2.add(ans3);list2.add(ans4);
		
		Question question1 = new Question();
		question1.setQname("What is Java");
		question1.setAnswers(list1);
		
		Question question2 = new Question();
		question2.setQname("What is Servlet");
		question2.setAnswers(list2);
		
		session.persist(question1);
		session.persist(question2);
		
		tx.commit();
		session.close();
		System.out.println("Success");
		
		
		
		
		
	}
}
