package org.dxc;

import org.dxc.entity.Address;
import org.dxc.entity.Employee;
import org.hibernate.*;

import org.hibernate.cfg.Configuration;

public class App {
	private static SessionFactory factory;

	public static void main(String[] args) {

		try {
			// factory obj created
			factory = new Configuration().configure("resource/hibernate.cfg.xml").buildSessionFactory();
		} catch (Throwable e) {
			System.err.println("Failed to create Session");
			throw new ExceptionInInitializerError(e);
		}
		Session session = factory.openSession();
		Transaction tx = session.beginTransaction();

		Employee e1 = new Employee();
		e1.setName("Ravi Malik");
		e1.setEmail("ravi@gmail.com");
		
		Employee e2 = new Employee();
		e2.setName("Damien");
		e2.setEmail("damien@gmail.com");

		Address address1 = new Address();
		address1.setAddressLine1("G-21,Lohia nagar");
		address1.setCity("Ghaziabad");
		address1.setState("UP");
		address1.setCountry("India");
		address1.setPincode(201301);
		
		Address address2 = new Address();
		address2.setAddressLine1("Choa Chu Kang");
		address2.setCity("Singapore");
		address2.setState("Northwest");
		address2.setCountry("Singapore");
		address2.setPincode(680447);

		e1.setAddress(address1);
		address1.setEmployee(e1);
		
		e2.setAddress(address2);
		address2.setEmployee(e2);

		session.persist(e1);
		session.persist(e2);

		tx.commit();

		session.close();
		System.out.println("success");
	}
}